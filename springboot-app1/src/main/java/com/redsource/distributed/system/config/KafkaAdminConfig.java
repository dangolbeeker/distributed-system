package com.redsource.distributed.system.config;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Configuration
public class KafkaAdminConfig {
    @Value("${kafka.bootstrap-address}")
    private String bootstrapAddress;

    @Bean
    public AdminClient adminClient() {
        final Map<String, Object> props = Map.of(
                AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress,
                AdminClientConfig.RETRIES_CONFIG, 5);

        return AdminClient.create(props);
    }

}
