# distributed-system

This is shell project for creating highthrought put system with the use of SpringFramework microservice & Kafka for events

## Deploying on local

**run the following command to Initialize Kafka**

`docker-compose -f docker/kafka-zoo-singlenode.yml up`

**run the following command to initilize mongoDB and mongoDB express**

`docker-compose -f docker/mongodb-express.yml up`

**To run the spring boot application**

`cd springboot-app1`

`./gradlew bootRun`

**To publishMessage to Topic**

`http://localhost:8080/trigger/{yourMessage}`


## Running on Kubernetes

** This feature will be added in the future **
